package engine.opengl;

import org.lwjgl.opengl.GL;

import static org.lwjgl.glfw.GLFW.*;
import static org.lwjgl.opengl.GL31.*;

public class GraphicsContext {
    private final long m_Window;

    public GraphicsContext(long window) {
        this.m_Window = window;
    }

    public void init() {
        glfwMakeContextCurrent(m_Window);
        GL.createCapabilities();

        System.out.printf("[INFO] Vendor:   %s\n", glGetString(GL_VENDOR));
        System.out.printf("[INFO] Renderer: %s\n", glGetString(GL_RENDERER));
        System.out.printf("[INFO] Version:  %s\n", glGetString(GL_VERSION));
    }

    public void swapBuffers() {
        glfwSwapBuffers(m_Window);
    }

}
