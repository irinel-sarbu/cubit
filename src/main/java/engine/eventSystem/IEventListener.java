package engine.eventSystem;

public interface IEventListener {
    void onEvent(Event event);
}
