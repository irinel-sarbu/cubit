package engine.eventSystem;

import java.util.ArrayList;
import java.util.List;

public class Observable {
    private final List<IEventListener> m_ListenerList = new ArrayList<>();

    public synchronized void register(IEventListener listener) {
        m_ListenerList.add(listener);
    }

    public synchronized void notify(Event event) {
        m_ListenerList.forEach(listener -> listener.onEvent(event));
    }
}
