package engine.eventSystem;

@FunctionalInterface
public interface IEventHandler {
    boolean onEvent(Event event);
}
