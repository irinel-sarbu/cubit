package engine.eventSystem;

import java.io.Serializable;
import java.util.UUID;

public class Event implements Serializable {
    protected boolean handled;
    private final IEventType m_Type;
    private final UUID m_UUID;

    protected Event(IEventType type) {
        this.m_Type = type;
        this.m_UUID = UUID.randomUUID();
    }

    public IEventType getType() {
        return m_Type;
    }

    public boolean isHandled() {
        return handled;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Event event = (Event) o;
        return m_UUID.equals(event.m_UUID);
    }

    @Override
    public int hashCode() {
        return m_UUID.hashCode();
    }
}
