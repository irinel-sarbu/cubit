package engine.eventSystem.types;

import engine.eventSystem.IEventType;

public enum CoreEventTypes implements IEventType {
    WINDOW_CLOSE,

    KEY_PRESSED,
    KEY_RELEASED,
    KEY_TYPED
}
