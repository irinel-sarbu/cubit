package engine.eventSystem.types.application;

import engine.eventSystem.Event;

import static engine.eventSystem.types.CoreEventTypes.WINDOW_CLOSE;

public final class WindowCloseEvent extends Event {
    public WindowCloseEvent() {
        super(WINDOW_CLOSE);
    }
}
