package engine.eventSystem.types.key;

import engine.core.KeyCode;
import engine.eventSystem.Event;
import engine.eventSystem.IEventType;

public abstract class KeyEvent extends Event {
    private final KeyCode m_KeyCode;
    protected KeyEvent(IEventType type, KeyCode keyCode) {
        super(type);
        this.m_KeyCode = keyCode;
    }

    public KeyCode getKeyCode() {
        return m_KeyCode;
    }
}
