package engine.eventSystem.types.key;

import engine.core.KeyCode;

import static engine.eventSystem.types.CoreEventTypes.KEY_RELEASED;

public final class KeyReleasedEvent extends KeyEvent {
    public KeyReleasedEvent(KeyCode keyCode) {
        super(KEY_RELEASED, keyCode);
    }
}
