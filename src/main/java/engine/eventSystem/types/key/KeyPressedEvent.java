package engine.eventSystem.types.key;

import engine.core.KeyCode;

import static engine.eventSystem.types.CoreEventTypes.KEY_PRESSED;

public final class KeyPressedEvent extends KeyEvent {
    private final int m_RepeatCount;

    public KeyPressedEvent(KeyCode keyCode, int repeatCount) {
        super(KEY_PRESSED, keyCode);

        this.m_RepeatCount = repeatCount;
    }

    public int getRepeatCount() {
        return m_RepeatCount;
    }
}
