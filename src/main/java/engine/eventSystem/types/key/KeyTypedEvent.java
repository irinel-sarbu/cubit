package engine.eventSystem.types.key;

import engine.core.KeyCode;

import static engine.eventSystem.types.CoreEventTypes.KEY_TYPED;

public final class KeyTypedEvent extends KeyEvent {
    public KeyTypedEvent(KeyCode keyCode) {
        super(KEY_TYPED, keyCode);
    }
}
