package engine.eventSystem;

public class EventDispatcher {
    private final Event m_Event;

    public EventDispatcher(Event event) {
        this.m_Event = event;
    }

    public void dispatch(IEventType type, IEventHandler handler) {
        if(m_Event.handled)
            return;

        if(m_Event.getType() == type) {
            m_Event.handled |= handler.onEvent(m_Event);
        }
    }
}
