package engine.core;

import java.util.HashMap;
import java.util.Map;

public enum KeyCode {
    SPACE(32),

    A(65),
    B(66),
    C(67),
    D(68),
    E(69),
    F(70),
    G(71),
    H(72),
    I(73),
    J(74),
    K(75),
    L(76),
    M(77),
    N(78),
    O(79),
    P(80),
    Q(81),
    R(82),
    S(83),
    T(84),
    U(85),
    V(86),
    W(87),
    X(88),
    Y(89),
    Z(90),

    Escape(256),
    Enter(257),
    Tab(258),
    Backspace(259),
    Insert(260),
    Delete(261),
    Right(262),
    Left(263),
    Down(264),
    Up(265),
    PageUp(266),
    PageDown(267),
    Home(268),
    End(269),
    CapsLock(280),
    ScrollLock(281),
    NumLock(282),
    PrintScreen(283),
    Pause(284),
    F1(290),
    F2(291),
    F3(292),
    F4(293),
    F5(294),
    F6(295),
    F7(296),
    F8(297),
    F9(298),
    F10(299),
    F11(300),
    F12(301),
    F13(302),
    F14(303),
    F15(304),
    F16(305),
    F17(306),
    F18(307),
    F19(308),
    F20(309),
    F21(310),
    F22(311),
    F23(312),
    F24(313),
    F25(314);

    private final int m_Value;
    private static final Map<Integer, KeyCode> m_Lookup = new HashMap<>();

    static {
        for (KeyCode key : KeyCode.values()) {
            m_Lookup.put(key.getValue(), key);
        }
    }

    KeyCode(int value) {
        this.m_Value = value;
    }

    public int getValue() {
        return m_Value;
    }

    public static KeyCode get(int key) {
        return m_Lookup.get(key);
    }
}
