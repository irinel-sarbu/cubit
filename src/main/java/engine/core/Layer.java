package engine.core;

import engine.eventSystem.Event;
import engine.eventSystem.IEventListener;

public abstract class Layer implements IEventListener {
    private final String m_DebugName;

    public Layer(String debugName) {
        this.m_DebugName = debugName;
    }

    public abstract void onAttach();
    public abstract void onDetach();
    public abstract void onUpdate(TimeStep dt);
    public abstract void onEvent(Event event);

    public String getDebugName() {
        return m_DebugName;
    }

    @Override
    public String toString() {
        return m_DebugName;
    }
}
