package engine.core;

public final class Util {
    public static long Bit(short n) {
        return (long) Math.pow(2, n);
    }
}
