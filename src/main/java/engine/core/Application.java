package engine.core;

import engine.eventSystem.*;
import engine.eventSystem.types.application.WindowCloseEvent;
import engine.eventSystem.types.key.KeyPressedEvent;

import java.util.ArrayDeque;
import java.util.Deque;

import static engine.eventSystem.types.CoreEventTypes.*;
import static org.lwjgl.glfw.GLFW.glfwGetTime;

public abstract class Application implements IEventListener {
    private final Window m_Window;
    private final Deque<Layer> m_LayerStack;
    private double m_LastFrameTime = 0.0;
    private boolean m_Running = true;
    private boolean m_Minimized = false;

    public Application(String name, String[] args) {
        this.m_LayerStack = new ArrayDeque<>();
        this.m_Window = new Window(new WindowProps(name));
        this.m_Window.register(this);
    }

    public void pushLayer(Layer layer) {
        this.m_LayerStack.push(layer);
        layer.onAttach();
    }

    @Override
    public void onEvent(Event event) {
        EventDispatcher dispatcher = new EventDispatcher(event);

        dispatcher.dispatch(WINDOW_CLOSE, e -> onWindowClose((WindowCloseEvent) e));
        dispatcher.dispatch(KEY_PRESSED, e -> onKeyPressed((KeyPressedEvent) e));

        for(Layer layer : m_LayerStack) {
            if(event.isHandled()) break;
            layer.onEvent(event);
        }
    }

    public void run() {
        while(m_Running) {
            double time = glfwGetTime();
            TimeStep dt = new TimeStep(time - m_LastFrameTime);
            m_LastFrameTime = time;

            if(!m_Minimized) {
                m_LayerStack.forEach(layer -> layer.onUpdate(dt));
            }

            m_Window.onUpdate();
        }
    }

    public void destroy() {
        m_Window.shutdown();
    }

    @EventHandler
    private boolean onWindowClose(WindowCloseEvent event) {
        System.out.println("[DEBUG] received window close event");
        m_Running = false;
        return true;
    }

    @EventHandler
    private boolean onKeyPressed(KeyPressedEvent event) {
        if(event.getKeyCode() == KeyCode.Escape) {
            System.out.println("[DEBUG] user pressed Escape. Closing window");
            m_Running = false;
            return true;
        }
        return false;
    }
}
