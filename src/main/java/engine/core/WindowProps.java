package engine.core;

final class WindowProps {
    private final String m_Title;
    private final int m_Width;
    private final int m_Height;

    public WindowProps(String title) {
        this.m_Title = title;
        this.m_Width = 1280;
        this.m_Height = 720;
    }

    public WindowProps(String title, int width, int height) {
        this.m_Title = title;
        this.m_Width = width;
        this.m_Height = height;
    }

    public String getTitle() {
        return m_Title;
    }

    public int getWidth() {
        return m_Width;
    }

    public int getHeight() {
        return m_Height;
    }
}
