package engine.core;

public final class TimeStep {
    private final double m_Dt;

    public TimeStep(double dt) {
        this.m_Dt = dt;
    }

    public double getSeconds() {
        return m_Dt;
    }

    public double getMilliseconds() {
        return m_Dt * 1000.0;
    }
}
