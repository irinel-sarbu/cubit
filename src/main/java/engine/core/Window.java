package engine.core;

import engine.eventSystem.Observable;
import engine.eventSystem.types.application.WindowCloseEvent;
import engine.eventSystem.types.key.KeyPressedEvent;
import engine.eventSystem.types.key.KeyReleasedEvent;
import engine.eventSystem.types.key.KeyTypedEvent;
import engine.opengl.GraphicsContext;

import static org.lwjgl.glfw.GLFW.*;
import static org.lwjgl.system.MemoryUtil.NULL;

public final class Window extends Observable {
    private long m_Window;
    private GraphicsContext m_Context;

    private int m_Width, m_Height;
    private String m_Title;

    private boolean m_Vsync = false;

    public Window(WindowProps props) {
        init(props);
    }

    private void init(WindowProps props) {
        this.m_Title = props.getTitle();
        this.m_Width = props.getWidth();
        this.m_Height = props.getHeight();

        if (!glfwInit())
            throw new IllegalStateException("Unable to initialize GLFW");

        m_Window = glfwCreateWindow(this.m_Width, this.m_Height, this.m_Title, NULL, NULL);
        if (m_Window == NULL)
            throw new RuntimeException("Failed to create the GLFW m_Window");

        m_Context = new GraphicsContext(m_Window);
        m_Context.init();

        setVsync(true);

        glfwSetWindowCloseCallback(m_Window, (window) -> notify(new WindowCloseEvent()));

        glfwSetCharCallback(m_Window, (window, key) -> notify(new KeyTypedEvent(KeyCode.get(key))));
        glfwSetKeyCallback(m_Window, (window, key, scancode, action, mods) -> {
            switch (action) {
                case GLFW_PRESS -> notify(new KeyPressedEvent(KeyCode.get(key), 0));
                case GLFW_RELEASE -> notify(new KeyReleasedEvent(KeyCode.get(key)));
                case GLFW_REPEAT -> notify(new KeyPressedEvent(KeyCode.get(key), 1));
            }
        });
    }

    public void shutdown() {
        glfwDestroyWindow(m_Window);
        glfwTerminate();
    }

    public void onUpdate() {
        glfwPollEvents();
        m_Context.swapBuffers();
    }

    public void setVsync(boolean enabled) {
        this.m_Vsync = enabled;

        glfwSwapInterval(enabled ? 1 : 0);
    }

    public boolean isVsync() {
        return m_Vsync;
    }
}
