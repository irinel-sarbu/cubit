package application.eventSystem.types;

import application.eventSystem.CustomEventType;
import engine.eventSystem.Event;

public class TestEvent extends Event {
    private final String msg;

    public TestEvent(String msg) {
        super(CustomEventType.TEST_EVENT);
        this.msg = msg;
    }

    public String getMessage() {
        return msg;
    }
}
