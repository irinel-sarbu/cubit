package application.eventSystem.types;

import application.eventSystem.CustomEventType;
import engine.eventSystem.Event;

public class TestEvent2 extends Event {
    private final String msg;

    public TestEvent2(String msg) {
        super(CustomEventType.TEST_EVENT_2);
        this.msg = msg;
    }

    public String getMessage() {
        return msg;
    }
}
