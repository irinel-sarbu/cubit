package application;

import application.eventSystem.CustomEventType;
import application.eventSystem.types.TestEvent;
import application.eventSystem.types.TestEvent2;
import engine.core.Layer;
import engine.core.TimeStep;
import engine.eventSystem.Event;
import engine.eventSystem.EventDispatcher;

public class TestLayer2 extends Layer {

    public TestLayer2() {
        super("testLayer2");
    }

    @Override
    public void onAttach() {

    }

    @Override
    public void onDetach() {

    }

    @Override
    public void onUpdate(TimeStep dt) {

    }

    @Override
    public void onEvent(Event event) {
        EventDispatcher dispatcher = new EventDispatcher(event);

        dispatcher.dispatch(CustomEventType.TEST_EVENT, e -> onTestEvent((TestEvent) e));
        dispatcher.dispatch(CustomEventType.TEST_EVENT_2, e -> onTestEvent2((TestEvent2) e));
    }

    private boolean onTestEvent(TestEvent event) {
        System.out.println("[DEBUG] " + this + " " + event.getMessage());
        return true;
    }

    private boolean onTestEvent2(TestEvent2 event) {
        System.out.println("[DEBUG] " + this + " " + event.getMessage());
        return true;
    }
}
