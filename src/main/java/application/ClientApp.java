package application;

import application.eventSystem.types.*;
import engine.core.Application;

public class ClientApp extends Application {
    public ClientApp(String[] args) {
        super("ClientAPP", args);
        pushLayer(new TestLayer());
        pushLayer(new TestLayer2());

        onEvent(new TestEvent("TestEvent1"));
        onEvent(new TestEvent2("TestEvent2"));
    }

    public static void main(String[] args) {
        Application app = new ClientApp(args);
        app.run();
        app.destroy();
    }
}
