package application;

import application.eventSystem.CustomEventType;
import application.eventSystem.types.TestEvent;
import engine.core.Layer;
import engine.core.TimeStep;
import engine.eventSystem.Event;
import engine.eventSystem.EventDispatcher;

public class TestLayer extends Layer {

    public TestLayer() {
        super("testLayer1");
    }

    @Override
    public void onAttach() {

    }

    @Override
    public void onDetach() {

    }

    @Override
    public void onUpdate(TimeStep dt) {

    }

    @Override
    public void onEvent(Event event) {
        EventDispatcher dispatcher = new EventDispatcher(event);

        dispatcher.dispatch(CustomEventType.TEST_EVENT, e -> onTestEvent((TestEvent) e));
    }

    private boolean onTestEvent(TestEvent event) {
        System.out.println("[DEBUG] " + this + event.getMessage());
        return true;
    }
}
